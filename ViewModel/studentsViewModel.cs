﻿using Microsoft.AspNetCore.Http;
using SchoolMGT.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace SchoolMGT.ViewModel
{
    public class studentsViewModel
    {
        [Key]
        public Guid studentId { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "Student Name field is required!")]
        public string studentName { get; set; }

        [MaxLength(1)]
        public string gender { get; set; }

        [Required(ErrorMessage = "Date Of Birth is required!")]
        [DataType(DataType.Date)]
        public DateTime dob { get; set; }

        [Required(ErrorMessage = "Place Of Birth is required!")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Place Of Birth")]
        public string pob { get; set; }

        [Required(ErrorMessage = "Current Place is required!")]
        [DataType(DataType.MultilineText)]
        public string currentPlace { get; set; }

        [Phone]
        [MaxLength(11)]
        [Required]
        public string phone { get; set; }

        [EmailAddress]
        public string email { get; set; }

        [MaxLength(200)]
        public string transferredFrom { get; set; }

        [MaxLength(100)]
        public string momName { get; set; }

        [MaxLength(100)]
        public string dadName { get; set; }

        [MaxLength(300)]
        [DataType(DataType.MultilineText)]
        public string noted { get; set; }

        public IFormFile ImagePath { get; set; }
    }
}
