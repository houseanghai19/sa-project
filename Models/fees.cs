﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolMGT.Models
{
    public class fees
    {
        [Key]
        public Guid feeId { get; set; }

        [Required(ErrorMessage ="Period is required!")]
        public string period { get; set; }

        [Required]
        public float amount { get; set; }

        [DataType(DataType.MultilineText)]
        public string descr { get; set; }


    }
}
