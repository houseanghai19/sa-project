﻿using SchoolMGT.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;


namespace SchoolMGT.Models
{
    public class students
    {
        [Key]
        public Guid studentId { get; set; }

        [MaxLength(100)]
        [Required(ErrorMessage = "Student Name field is required!")]
        public string studentName { get; set; }

        [MaxLength(1)]
        public string gender { get; set; }

        [Required(ErrorMessage ="Date Of Birth is required!")]
        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        public DateTime dob { get; set; }

        [Required(ErrorMessage = "Place Of Birth is required!")]
        [DataType(DataType.MultilineText)]
        [Display(Name ="Place Of Birth")]
        public string pob { get; set; }

        [Required(ErrorMessage = "Current Place is required!")]
        [DataType(DataType.MultilineText)]
        public string currentPlace { get; set; }

        [Phone]
        [MaxLength(11)]
        [Required]
        public string phone { get; set; }

        [EmailAddress]
        public string email { get; set; }

        [MaxLength(200)]
        public string transferredFrom { get; set; }

        [MaxLength(100)]
        public string momName { get; set; }

        [MaxLength(100)]
        public string dadName { get; set; }

        [MaxLength(300)]
        [DataType(DataType.MultilineText)]
        public string noted { get; set; }

        public string imagePart { get; set; }
    }
}
