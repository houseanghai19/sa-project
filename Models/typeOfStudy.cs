﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolMGT.Models
{
    public class typeOfStudy
    {
        [Key]
        public Guid typeOfStudy_Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string typeOfStudy_Name { get; set; }
    }
}
