﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolMGT.Models
{
    public class paymentType
    {
        [Key]
        public Guid paymentType_Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string paymentType_Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string descr { get; set; }
    }
}
