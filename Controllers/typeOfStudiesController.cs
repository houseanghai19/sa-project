﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolMGT.Data;
using SchoolMGT.Models;
using System.Linq.Dynamic.Core;
using SchoolMGT.ViewModel;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace SchoolMGT.Controllers
{
    public class typeOfStudiesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public typeOfStudiesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: typeOfStudies
        public IActionResult Index()
        {
            return View();
        }

        //Copy From other Controller
        public async Task<JsonResult> LoadData()
        {
            JsonResult result;
            try
            {
                var form = await HttpContext.Request.ReadFormAsync();
                var draw = form["draw"];
                var start = form["start"];
                var length = form["length"];
                var sortColumn = form["columns[" + form["order[0][column]"] + "][name]"];
                var sortColumnDirection = form["order[0][dir]"];
                var searchValue = form["search[value]"];
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int skip = start.ToString() != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var data1 = (from r in _context.typeOfStudy
                                 //join c in _context.classes on r.studentId equals c.classId
                                 //join u in _context.paymentType on r.studentId equals u.paymentTypeID
                             select new
                             {
                                 r.typeOfStudy_Id,
                                 r.typeOfStudy_Name,
                                
                             });

                //Search EF-LINQ Core 5
                if (!string.IsNullOrEmpty(searchValue))
                {
                    data1 = data1.Where(x => x.typeOfStudy_Name.StartsWith(searchValue));
                }
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    data1 = data1.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                recordsTotal = await data1.CountAsync();
                var data = await data1.Skip(skip).Take(pageSize).ToListAsync();
                result = Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch
            {
                result = null;
            }
            return result;
        }

        //End Copy

        // GET: typeOfStudies/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfStudy = await _context.typeOfStudy
                .FirstOrDefaultAsync(m => m.typeOfStudy_Id == id);
            if (typeOfStudy == null)
            {
                return NotFound();
            }

            return View(typeOfStudy);
        }

        // GET: typeOfStudies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: typeOfStudies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("typeOfStudy_Id,typeOfStudy_Name")] typeOfStudy typeOfStudy)
        {
            if (ModelState.IsValid)
            {
                
                var typeOfStudys = new typeOfStudy
                {
                    typeOfStudy_Id = Guid.NewGuid(),
                    typeOfStudy_Name = typeOfStudy.typeOfStudy_Name,                  
                };
                _context.Add(typeOfStudy);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(typeOfStudy);
        }

        // GET: typeOfStudies/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfStudy = await _context.typeOfStudy.FindAsync(id);
            if (typeOfStudy == null)
            {
                return NotFound();
            }
            return View(typeOfStudy);
        }

        // POST: typeOfStudies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("typeOfStudy_Id,typeOfStudy_Name")] typeOfStudy typeOfStudy)
        {
            if (id != typeOfStudy.typeOfStudy_Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(typeOfStudy);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!typeOfStudyExists(typeOfStudy.typeOfStudy_Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(typeOfStudy);
        }

        // GET: typeOfStudies/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeOfStudy = await _context.typeOfStudy
                .FirstOrDefaultAsync(m => m.typeOfStudy_Id == id);
            if (typeOfStudy == null)
            {
                return NotFound();
            }

            return View(typeOfStudy);
        }

        // POST: typeOfStudies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var typeOfStudy = await _context.typeOfStudy.FindAsync(id);
            _context.typeOfStudy.Remove(typeOfStudy);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool typeOfStudyExists(Guid id)
        {
            return _context.typeOfStudy.Any(e => e.typeOfStudy_Id == id);
        }
    }
}
