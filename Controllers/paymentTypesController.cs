﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolMGT.Data;
using SchoolMGT.Models;
using System.Linq.Dynamic.Core;
using SchoolMGT.ViewModel;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace SchoolMGT.Controllers
{
    public class paymentTypesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public paymentTypesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: paymentTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.paymentType.ToListAsync());
        }

        //Copy From other Controller
        public async Task<JsonResult> LoadData()
        {
            JsonResult result;
            try
            {
                var form = await HttpContext.Request.ReadFormAsync();
                var draw = form["draw"];
                var start = form["start"];
                var length = form["length"];
                var sortColumn = form["columns[" + form["order[0][column]"] + "][name]"];
                var sortColumnDirection = form["order[0][dir]"];
                var searchValue = form["search[value]"];
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int skip = start.ToString() != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var data1 = (from r in _context.paymentType
                                 //join c in _context.classes on r.studentId equals c.classId
                                 //join u in _context.paymentType on r.studentId equals u.paymentTypeID
                             select new
                             {
                                 r.paymentType_Id,
                                 r.paymentType_Name,
                                 r.descr,
                                 

                             });

                //Search EF-LINQ Core 5
                if (!string.IsNullOrEmpty(searchValue))
                {
                    data1 = data1.Where(x => x.paymentType_Name.StartsWith(searchValue));
                }
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    data1 = data1.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                recordsTotal = await data1.CountAsync();
                var data = await data1.Skip(skip).Take(pageSize).ToListAsync();
                result = Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch
            {
                result = null;
            }
            return result;
        }

        //End Copy

        // GET: paymentTypes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paymentType = await _context.paymentType
                .FirstOrDefaultAsync(m => m.paymentType_Id == id);
            if (paymentType == null)
            {
                return NotFound();
            }

            return View(paymentType);
        }

        // GET: paymentTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: paymentTypes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("paymentType_Id,paymentType_Name,descr")] paymentType paymentType)
        {
            if (ModelState.IsValid)
            {
               
                var paymentTypes = new paymentType
                {
                    paymentType_Id = Guid.NewGuid(),
                    paymentType_Name = paymentType.paymentType_Name,
                    descr=paymentType.descr,                   
                };
                _context.Add(paymentTypes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(paymentType);
        }

        // GET: paymentTypes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paymentType = await _context.paymentType.FindAsync(id);
            if (paymentType == null)
            {
                return NotFound();
            }
            return View(paymentType);
        }

        // POST: paymentTypes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("paymentType_Id,paymentType_Name,descr")] paymentType paymentType)
        {
            if (id != paymentType.paymentType_Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(paymentType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!paymentTypeExists(paymentType.paymentType_Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(paymentType);
        }

        // GET: paymentTypes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paymentType = await _context.paymentType
                .FirstOrDefaultAsync(m => m.paymentType_Id == id);
            if (paymentType == null)
            {
                return NotFound();
            }

            return View(paymentType);
        }

        // POST: paymentTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var paymentType = await _context.paymentType.FindAsync(id);
            _context.paymentType.Remove(paymentType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool paymentTypeExists(Guid id)
        {
            return _context.paymentType.Any(e => e.paymentType_Id == id);
        }
    }
}
