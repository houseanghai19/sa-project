﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolMGT.Data;
using SchoolMGT.Models;
using System.Linq.Dynamic.Core;
using SchoolMGT.ViewModel;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace SchoolMGT.Controllers
{
    public class feesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public feesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: fees
        public async Task<IActionResult> Index()
        {
            return View(await _context.fees.ToListAsync());
        }


        //Copy From other Controller
        public async Task<JsonResult> LoadData()
        {
            JsonResult result;
            try
            {
                var form = await HttpContext.Request.ReadFormAsync();
                var draw = form["draw"];
                var start = form["start"];
                var length = form["length"];
                var sortColumn = form["columns[" + form["order[0][column]"] + "][name]"];
                var sortColumnDirection = form["order[0][dir]"];
                var searchValue = form["search[value]"];
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int skip = start.ToString() != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var data1 = (from r in _context.fees
                                 //join c in _context.classes on r.studentId equals c.classId
                                 //join u in _context.paymentType on r.studentId equals u.paymentTypeID
                             select new
                             {
                                 r.feeId,
                                 r.period,
                                 r.amount,
                                 r.descr,
                                 

                             });

                //Search EF-LINQ Core 5
                if (!string.IsNullOrEmpty(searchValue))
                {
                    data1 = data1.Where(x => x.period.StartsWith(searchValue));
                }
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    data1 = data1.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                recordsTotal = await data1.CountAsync();
                var data = await data1.Skip(skip).Take(pageSize).ToListAsync();
                result = Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch
            {
                result = null;
            }
            return result;
        }

        //End Copy

        // GET: fees/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fees = await _context.fees
                .FirstOrDefaultAsync(m => m.feeId == id);
            if (fees == null)
            {
                return NotFound();
            }

            return View(fees);
        }

        // GET: fees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: fees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("feeId,period,amount,descr")] fees fees)
        {
            if (ModelState.IsValid)
            {
               
                var fee = new fees
                {
                    feeId = Guid.NewGuid(),
                    period = fees.period,
                    amount=fees.amount,
                    descr=fees.descr,
                    
                };
                _context.Add(fee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(fees);
        }

        // GET: fees/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fees = await _context.fees.FindAsync(id);
            if (fees == null)
            {
                return NotFound();
            }
            return View(fees);
        }

        // POST: fees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("feeId,period,amount,descr")] fees fees)
        {
            if (id != fees.feeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fees);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!feesExists(fees.feeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(fees);
        }

        // GET: fees/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fees = await _context.fees
                .FirstOrDefaultAsync(m => m.feeId == id);
            if (fees == null)
            {
                return NotFound();
            }

            return View(fees);
        }

        // POST: fees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var fees = await _context.fees.FindAsync(id);
            _context.fees.Remove(fees);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool feesExists(Guid id)
        {
            return _context.fees.Any(e => e.feeId == id);
        }
    }
}
