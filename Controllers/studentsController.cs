﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SchoolMGT.Data;
using SchoolMGT.Models;
using System.Linq.Dynamic.Core;
using SchoolMGT.ViewModel;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace SchoolMGT.Controllers
{
    public class studentsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public studentsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: students
        public async Task<IActionResult> Index()
        {
            return View(await _context.students.ToListAsync());
        }

        //Copy From other Controller
        public async Task<JsonResult> LoadData()
        {
            JsonResult result;
            try
            {
                var form = await HttpContext.Request.ReadFormAsync();
                var draw = form["draw"];
                var start = form["start"];
                var length = form["length"];
                var sortColumn = form["columns[" + form["order[0][column]"] + "][name]"];
                var sortColumnDirection = form["order[0][dir]"];
                var searchValue = form["search[value]"];
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int skip = start.ToString() != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                var data1 = (from r in _context.students
                                 //join c in _context.classes on r.studentId equals c.classId
                                 //join u in _context.paymentType on r.studentId equals u.paymentTypeID
                             select new
                             {
                                 r.studentId,
                                 r.studentName,
                                 r.gender,
                                 r.dob,
                                 r.pob,
                                 r.phone,
                                 r.email,
                                 r.transferredFrom,
                                 r.currentPlace,
                                 r.momName,
                                 r.dadName,
                                 r.noted,

                             });

                //Search EF-LINQ Core 5
                if (!string.IsNullOrEmpty(searchValue))
                {
                    data1 = data1.Where(x => x.studentName.StartsWith(searchValue));
                }
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    data1 = data1.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                recordsTotal = await data1.CountAsync();
                var data = await data1.Skip(skip).Take(pageSize).ToListAsync();
                result = Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch
            {
                result = null;
            }
            return result;
        }

        //End Copy

        // GET: students/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var students = await _context.students
                .FirstOrDefaultAsync(m => m.studentId == id);
            if (students == null)
            {
                return NotFound();
            }

            return View(students);
        }

        // GET: students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: students/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("studentId,studentName,gender,dob,pob,currentPlace,phone,email,transferredFrom,momName,dadName,noted,imagePart")] students students)
        {
            if (ModelState.IsValid)
            {
                // students.studentId = Guid.NewGuid();
                var student = new students
                {
                    studentId = Guid.NewGuid(),
                    studentName = students.studentName,
                    gender = students.gender,
                    dob=students.dob,
                    dadName = students.dadName,
                    momName = students.momName,
                    currentPlace = students.currentPlace,
                    email = students.email,
                    noted = students.noted,
                    phone = students.phone,
                    pob = students.pob,
                    transferredFrom = students.transferredFrom,
                  //  imagePart = students.imagePart,
                };
                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(students);
        }

        // GET: students/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var students = await _context.students.FindAsync(id);
            if (students == null)
            {
                return NotFound();
            }
            return View(students);
        }

        // POST: students/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("studentId,studentName,gender,dob,pob,currentPlace,phone,email,transferredFrom,momName,dadName,noted,imagePart")] students students)
        {
            if (id != students.studentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(students);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!studentsExists(students.studentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(students);
        }

        // GET: students/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var students = await _context.students
                .FirstOrDefaultAsync(m => m.studentId == id);
            if (students == null)
            {
                return NotFound();
            }

            return View(students);
        }

        // POST: students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var students = await _context.students.FindAsync(id);
            _context.students.Remove(students);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool studentsExists(Guid id)
        {
            return _context.students.Any(e => e.studentId == id);
        }
    }
}
