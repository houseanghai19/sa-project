﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using SchoolMGT.Models;

namespace SchoolMGT.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }
        public DbSet<students> students { get; set; }

        public DbSet<fees> fees { get; set; }

        public DbSet<typeOfStudy> typeOfStudy { get; set; }

        public DbSet<paymentType> paymentType { get; set; }
    }
}
