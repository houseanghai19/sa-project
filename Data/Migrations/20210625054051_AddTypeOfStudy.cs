﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolMGT.Data.Migrations
{
    public partial class AddTypeOfStudy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "typeOfStudy",
                columns: table => new
                {
                    typeOfStudy_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    typeOfStudy_Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_typeOfStudy", x => x.typeOfStudy_Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "typeOfStudy");
        }
    }
}
