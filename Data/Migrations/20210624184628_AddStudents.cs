﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolMGT.Data.Migrations
{
    public partial class AddStudents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "students",
                columns: table => new
                {
                    studentId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    studentName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    gender = table.Column<string>(type: "nvarchar(1)", maxLength: 1, nullable: true),
                    dob = table.Column<DateTime>(type: "datetime2", nullable: false),
                    pob = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    currentPlace = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    phone = table.Column<string>(type: "nvarchar(11)", maxLength: 11, nullable: false),
                    email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    transferredFrom = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    momName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    dadName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    noted = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    imagePart = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_students", x => x.studentId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "students");
        }
    }
}
