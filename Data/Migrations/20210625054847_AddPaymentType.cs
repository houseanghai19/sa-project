﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SchoolMGT.Data.Migrations
{
    public partial class AddPaymentType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "paymentType",
                columns: table => new
                {
                    paymentType_Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    paymentType_Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    descr = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_paymentType", x => x.paymentType_Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "paymentType");
        }
    }
}
